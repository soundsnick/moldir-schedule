Rails.application.routes.draw do
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  root 'ticket#index'
  get 'priorities', to: 'ticket#priorities', as: :priorities

  get 'create/board', to: 'ticket#board_create', as: :create_board_page
  get 'create/ticket', to: 'ticket#ticket_create', as: :create_ticket_page
  post 'create/board', to: 'ticket#board_create_back', as: :create_board
  post 'create/ticket', to: 'ticket#ticket_create_back', as: :create_ticket

  get 'share/ticket/:id', to: 'ticket#share_ticket_page', as: :share_ticket_page
  post 'share/ticket/:id', to: 'ticket#share_ticket', as: :share_ticket

  get 'login', to: 'user#login', as: :login
  get 'register', to: 'user#register', as: :register
  get 'logout', to: 'user#logout', as: :logout

  post 'login', to: 'user#login_back', as: :login_back
  post 'register', to: 'user#register_back', as: :register_back

  get 'delete/ticket/:id', to: 'ticket#delete_ticket', as: :delete_ticket
  get 'delete/board/:id', to: 'ticket#delete_board', as: :delete_board

end
