# Installation guide
* Install Docker and Docker-compose
* Build images and launch containers
```bash
$ docker-compose up -d
```
* Init database, migrate Models and seed essential initial values.
```bash
$ docker-compose run web rake db:create
$ docker-compose run web rake db:migrate
$ docker-compose run web rake db:seed
```

* Since you reloaded your machine, you don't have to do everything again. Just launch your app using command `docker-compose start`