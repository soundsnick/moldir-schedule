module ApplicationHelper
  def auth
    session[:auth]
  end

  def format_date(date)
    d = date.in_time_zone("Almaty")
    d.strftime("%d.%m.%Y") == DateTime.now.in_time_zone("Almaty").strftime("%d.%m.%Y") ? "Сегодня в #{d.strftime("%H:%M")}" : d.strftime("%d.%m.%Y %H:%M")
  end
end
