class UserController < ApplicationController
  def login_back
    if @user = User.find_by(email: params[:email], password: params[:password])
      session[:auth] = @user
      redirect_to root_path
    else
      redirect_to login_path, notice: "Неверная почта или пароль"
    end
  end

  def register_back
    if User.find_by(email: params[:email]).nil?
      @user = User.new
      @user.email = params[:email]
      @user.password = params[:password]
      @user.name = params[:name]
      @user.last_name = params[:last_name]
      if @user.save
        session[:auth] = @user
        redirect_to root_path
      else
        redirect_to register_path, notice: "Ошибка сервера. Попробуйте позже"
      end
    else
      redirect_to register_path, notice: "Пользователь с такой почтой уже зарегистрирован"
    end
  end

  def logout
    session[:auth] = nil
    redirect_to root_path
  end
end
