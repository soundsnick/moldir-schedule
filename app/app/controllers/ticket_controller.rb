class TicketController < ApplicationController
  def index
    if auth
      @boards = Board.where(user_id: auth['id'])
    else
      redirect_to login_path
    end
  end

  def priorities
    if auth
      @priorities = Priority.all
      @tickets = Ticket.where(user_id: auth['id'])
      render 'priorities'
    else
      redirect_to login_path
    end
  end

  def board_create_back
    @board = Board.new
    @board.title = params[:title]
    @board.user_id = auth['id']
    @board.save
    redirect_to root_path
  end

  def ticket_create
    @priorities = Priority.all
    @boards = Board.where(user_id: auth['id'])
  end

  def ticket_create_back
    @ticket = Ticket.new
    @ticket.title = params[:title]
    @ticket.content = params[:content]
    @ticket.user_id = auth['id']
    @ticket.priority_id = params[:priority_id]
    @ticket.board_id = params[:board_id]
    @ticket.assignee_date = params[:assignee_date]
    @ticket.tags = params[:tags]
    @ticket.save
    redirect_to root_path
  end

  def delete_ticket
    if @ticket = Ticket.find_by(id: params[:id]) and @ticket.user_id == auth['id']
      @ticket.destroy
      redirect_to root_path, notice: "Успешно удалено!"
    else
      redirect_to root_path, notice: "У вас нет на это прав"
    end
  end

  def delete_board
    if @ticket = Board.find_by(id: params[:id]) and @ticket.user_id == auth['id']
      @ticket.destroy
      redirect_to root_path, notice: "Успешно удалено!"
    else
      redirect_to root_path, notice: "У вас нет на это прав"
    end
  end

  def share_ticket_page
    render 'share'
  end

  def share_ticket
    if Share.find_by(ticket_id: params[:ticket_id], user_id: params[:user_id])
      redirect_to root_path, notice: "Вы уже делитесь с этим пользователем"
    else
      @share = Share.new
      @share.user_id = params[:user_id]
      @share.ticket_id = params[:ticket_id]
      @share.save
      redirect_to root_path, notice: "Теперь этот пользователь видит вашу задачу!"
    end
  end

end
