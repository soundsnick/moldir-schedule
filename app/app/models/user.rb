class User < ApplicationRecord
  has_many :tickets
  has_many :boards
  has_many :shares
end
