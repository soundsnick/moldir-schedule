class Priority < ApplicationRecord
  def self.get_title(id)
    Priority.find_by(id: id).title
  end
end
