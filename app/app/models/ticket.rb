class Ticket < ApplicationRecord
  belongs_to :user
  belongs_to :priority
  belongs_to :board
  has_many :shares

end
