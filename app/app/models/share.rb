class Share < ApplicationRecord
  belongs_to :ticket
  belongs_to :user

  def self.get_by_ticket(id)
    Share.where(ticket_id: id)
  end

  def self.get_by_user(id)
    Ticket.where(id: Share.where(user_id: id).select(:ticket_id))
  end
end
