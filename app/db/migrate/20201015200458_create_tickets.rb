class CreateTickets < ActiveRecord::Migration[6.0]
  def change
    create_table :tickets do |t|
      t.string :title
      t.text :content
      t.datetime :assignee_date
      t.integer :priority_id
      t.integer :user_id
      t.text :tags
      t.timestamps
    end
  end
end
